/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 2ndyrGroupB
 */
public class Shape {
    boolean filled;
    String color;
    public boolean Color;
public Shape() {
     filled = true;
     color = "green";
  }
public Shape(String color, boolean filled) {
      filled = true;
      color = "green";
  }
public String setColor() {
    return color;
  }
public String getColor() {
    return color;
  }
public boolean isFilled()
  {
      return filled == true;
  }
  @Override
 public String toString()
  {
      String isNot = "";
      if(isFilled() == false)
      {
          isNot = "not";
       }
          return "A Shape with color of " + color + " and is " + isNot + " filled. ";
       }

   
 //class Circle
 public class Circle extends Shape
{
  private double radius;

  public Circle()
  {
     super();
     radius = 1.0;
  }

  public Circle(double radius)
  {
     super();
     this.radius = radius;
  }

  public Circle(double radius, String color, boolean filled)
  {
     super (color, filled);
     this.radius = radius;

  }

  public double getRadius()
  {
    return radius;
  }

  public void setRadius(double radius)
  {
    this.radius = radius;
  }

  public double getArea()
  {
     return radius*radius*Math.PI;
  }

  public double getPerimeter()
  {
      return 2*radius*Math.PI;
   }
 
  public String toString()
  {
     return "A Circle with radius = " + radius + ", which is a subclass of " + super.toString();
  }
}
 //Class Rectangle
 public class Rectangle extends Shape
{
  private double width;
  private double length;

  public Rectangle()
  {
     super();
     width = 1.0;
     length = 1.0;
  }

  public Rectangle(double width, double length)
  {
     super();
     this.width = width;
     this.length = length;
  }

  public Rectangle(double width, double length, String color, boolean filled)
  {
     super (color, filled);
     this.width = width;
     this.length = length;

  }

  public double getWidth()
  {
    return width;
  }

  public void setWidth(double width)
  {
    this.width = width;
  }

  public double getLength()
  {
    return length;
  }

  public void setLength(double length)
  {
    this.length = length;
  }

  public double getArea()
  {
     return length*width;
  }

  public double getPerimeter()
  {
      return (2*length)+(2*width);
  }
 }
}

